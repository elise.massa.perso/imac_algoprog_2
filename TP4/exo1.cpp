#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

#include <stdlib.h>     /* srand, rand */
 

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return (2 * nodeIndex + 1);
}

int Heap::rightChild(int nodeIndex)
{
    return (2 * nodeIndex + 2);
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
	(*this)[i] = value;
	while (i > 0 && ((*this)[i] > (*this)[(i - 1)/2])){
		swap((i - 1)/2, i);
		i = (i - 1)/2;
	}
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;

	int l = leftChild(nodeIndex);
	
 
    if (l < heapSize && (*this)[l] > (*this)[i_max]) {
        i_max = l;
    }

	int r = rightChild(nodeIndex);

    if (r < heapSize && (*this)[r] > (*this)[i_max]) {
        i_max = r;
    }

	if(i_max != nodeIndex){
		swap(nodeIndex, i_max);
		heapify(heapSize, i_max);
	}
}

void Heap::buildHeap(Array& numbers)
{
	int heapSize = numbers.size();

	for(int i = 0; i < heapSize; i++){
		(*this)[i] = numbers[i];
		insertHeapNode(i, numbers[i]);
	}	
} 

void Heap::heapSort()
{
	
	//buildHeap(numbers);
	for (int j = this->size() - 1; j >= 0; j--){
		swap(0, j);
		heapify(j, 0);
	}

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
