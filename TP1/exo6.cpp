#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int nbVal;
    int capacite;
    // your code
};


void initialise(Liste* liste)
{
    liste->premier = NULL;
}

bool est_vide(const Liste* liste)
{
    if (liste->premier == NULL){
        return true;
    }
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud *nouveau = new Noeud;
    if (liste == NULL || nouveau == NULL){
        exit(EXIT_FAILURE);
    }
    nouveau->donnee = valeur;
    nouveau->suivant = liste->premier;
    liste->premier = nouveau;
}

void affiche(const Liste* liste)
{
    Noeud *current = liste->premier;
    while (current != NULL){
        cout << current->donnee << endl;
        current = current->suivant;
    }
}


int recupere(const Liste* liste, int n)
{
    int index = 0;
    Noeud *current = liste->premier;
    while (index < n){
        current = current->suivant;
        index ++;
    }
    int nDonnee = current->donnee;
    return nDonnee;
}

int cherche(const Liste* liste, int valeur)
{
    int index = 0;
    Noeud *current = liste->premier;
    while (current != NULL){
        if(current->donnee == valeur){
            return index;
    }
        index ++;
        current = current->suivant;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
  int index = 0;  
  Noeud *current = liste->premier;
  while (index < n){
      current = current ->suivant;
      index ++;
  }
  current->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->capacite <= tableau->nbVal + 1){
        tableau->capacite *= 2;
        tableau->donnees = (int*)realloc(tableau->donnees, tableau->capacite*sizeof(int));
    }
    tableau->donnees[tableau->nbVal] = valeur;
    tableau->nbVal += 1;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->nbVal = 0 ;
    tableau->capacite = capacite;
    tableau->donnees = (int*)malloc(sizeof(int)*capacite);
}

bool est_vide(const DynaTableau* liste)
{
    return liste->nbVal == 0;
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i < tableau->nbVal; i++){
        cout << tableau->donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if (n  < tableau->nbVal && tableau->nbVal > 0){
        return tableau->donnees[n];
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int index = 0;
    for (int i = 0; i < tableau->nbVal; i++){
        if (tableau->donnees[i] == valeur){
            return index;
        }
        index ++;
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if (n  < tableau->nbVal && tableau->nbVal > 0){
        tableau->donnees[n] == valeur;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud *nouveau = new Noeud;
    nouveau->donnee = valeur;
    nouveau->suivant = NULL;
    if (liste->premier != NULL){ /*si la file n'est pas vide*/
        Noeud *current = liste->premier;
        while (current->suivant != NULL){
            current = current->suivant;
        }
        current->suivant = nouveau;
    }
    else{ /*si la vide est vide*/
        liste->premier = nouveau;
    }
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    int donneeDefile = 0;
    if (liste->premier != NULL){
        Noeud *noeudDefile = liste->premier;
        donneeDefile = noeudDefile->donnee;
        liste->premier = noeudDefile->suivant;
    }
    return donneeDefile;
    return 0;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud *nouveau = new Noeud;
    nouveau->donnee = valeur;
    nouveau->suivant = NULL;
    if (liste->premier != NULL){ /*si la file n'est pas vide*/
        Noeud *current = liste->premier;
        while (current->suivant != NULL){
            current = current->suivant;
        }
        current->suivant = nouveau;
    }
    else{ /*si la vide est vide*/
        liste->premier = nouveau;
    }
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    int donneeDepile = 0;
    Noeud *noeudDepile = liste->premier;
    Noeud *current = liste->premier;
    while (current->suivant->suivant != NULL){
        current = current->suivant;
    }
    donneeDepile = current->suivant->donnee;
    current->suivant = NULL;
    return donneeDepile;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
