#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
    int stock;
	for(int i = 0; i < toSort.size() - 1; i ++){
        int im; 
        for (int j = i + 1; j <toSort.size(); j ++){
            if (toSort[im] > toSort[j]){
                im = j;
            }
        }
        if (im != i){
            stock = toSort[i];
            toSort[i] = toSort[im];
            toSort[im] = stock;
        }
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
